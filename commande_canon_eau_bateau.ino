/*---------------------------------------------------------------------------------------------
envoie d un message osc de type  " /arriere 0 " et " /arriere 1 "

  --------------------------------------------------------------------------------------------- */
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <OSCMessage.h>
#include <OSCBundle.h>
#include <OSCData.h>


//DHCP Ip
//char ssid[] = "GildasRouter";          // your network SSID (name)
//char pass[] = "987654321";                    // your network password

String ssid = "gildas";
String password = "20070512";


// A UDP instance to let us send and receive packets over UDP
WiFiUDP Udp;
//const IPAddress outIp(192, 168, 43, 228);     // remote IP (not needed for receive)
///const unsigned int outPort = 9999;          // remote port (not needed for receive)
const unsigned int localPort = 9999;        // local port to listen for UDP packets (here's where we send the packets)


OSCErrorCode error;

const int ar = 16, mil = 5, av = 4, etrave = 14;
const int arB = 12, arT = 13, avB = 0, avT = 2;



unsigned long previousMillis = 0;        // will store last time LED was updated
const long interval = 3000;           // interval at which to blink (milliseconds)



void setup() {

  pinMode(ar, OUTPUT);
  pinMode(mil , OUTPUT);
  pinMode(etrave , OUTPUT);
  pinMode(av , OUTPUT);
  pinMode(arB , OUTPUT);
  pinMode(arT, OUTPUT);
  pinMode(avB , OUTPUT);
  pinMode(avT, OUTPUT);

  void Arreter ();

  IPAddress ip(192, 168, 43, 100);
  IPAddress subnet(255, 255, 255, 0);
  IPAddress gt(192, 168, 43, 1);
  Serial.begin(9600);
  char charBuf[50];
  char charBuf2[50];
  ssid.toCharArray(charBuf, 50);
  password.toCharArray(charBuf2, 50);
  WiFi.config(ip, gt, subnet);
  WiFi.begin(charBuf , charBuf2);

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  Serial.println("Starting UDP");
  Udp.begin(localPort);
  Serial.print("Local port: ");
  Serial.println(Udp.localPort());

}



void loop() {

  unsigned long currentMillis = millis();

  if (currentMillis - previousMillis >= interval) {
    void Arreter ();
    previousMillis = currentMillis;
    Serial.println("void arreter");
  }

  OSCMessage msg;                       // OSCBundle bundle;

  int size = Udp.parsePacket();

  if (size > 0) {
    while (size--) {
      msg.fill (Udp.read());            //bundle.fill(Udp.read());
    }
    if (!msg.hasError()) {              //  if (!bundle.hasError()) {
      Serial.println("message ");
      msg.dispatch("/arriere", Arriere);
      msg.dispatch("/milieu", Milieu);
      msg.dispatch("/avant", Avant);
      msg.dispatch("/arriereB", ArriereB);
      msg.dispatch("/arriereT", ArriereT);
      msg.dispatch("/avantB", AvantB);
      msg.dispatch("/avantT", AvantT);
      msg.dispatch("/etrave", Etrave);
      previousMillis = currentMillis;

    } else {
      error = msg.getError();           //  error = bundle.getError();
      Serial.print("error: ");
      //      Serial.println(error);
    }
  }
  if (currentMillis - previousMillis >= interval) {
    previousMillis = currentMillis;
  }
}



