/*
int ar = 16, mil = 5, av = 4, arB = 14;
int arT = 12, avB = 13, avT = 0, etrave = 2;

pinMode(ar, OUTPUT);
pinMode(mil , OUTPUT);
pinMode(av , OUTPUT);
pinMode(arB , OUTPUT);
pinMode(arT, OUTPUT);
pinMode(avB , OUTPUT);
pinMode(avT, OUTPUT);
pinMode(etrave , OUTPUT);

msg.dispatch("/arriere", Arriere);
msg.dispatch("/milieu", Milieu);
msg.dispatch("/avant", Avant);
msg.dispatch("/arriereB", ArriereB);
msg.dispatch("/arriereT", ArriereT);
msg.dispatch("/avantB", ArriereB);
msg.dispatch("/avantT", ArriereT);
msg.dispatch("/etrave", Etrave);
 */



void Arriere(OSCMessage &msg) {
  int state = msg.getInt(0);
  digitalWrite(ar, state );
  Serial.print("/arriere");
  Serial.println(state);
}

void Milieu(OSCMessage &msg) {
  int state = msg.getInt(0);
  digitalWrite(mil, state );
  Serial.print("/milieu ");
  Serial.println(state);
}

void Avant(OSCMessage &msg) {
  int state = msg.getInt(0);
  digitalWrite(av, state );
  Serial.print("/avant ");
  Serial.println(state);
}

void ArriereB(OSCMessage &msg) {
  int state = msg.getInt(0);
  digitalWrite(arB, state );
  Serial.print("/arriereBabord ");
  Serial.println(state);
}

void ArriereT(OSCMessage &msg) {
  int state = msg.getInt(0);
  digitalWrite(arT, state );
  Serial.print("/arriereTribord ");
  Serial.println(state);
}

void AvantB(OSCMessage &msg) {
  int state = msg.getInt(0);
  digitalWrite(avB, state );
  Serial.print("/avantBabord ");
  Serial.println(state);
}

void AvantT(OSCMessage &msg) {
  int state = msg.getInt(0);
  digitalWrite(avT, state );
  Serial.print("/avantTribord ");
  Serial.println(state);
}

void Etrave(OSCMessage &msg) {
  int state = msg.getInt(0);
  digitalWrite(etrave, state );
  Serial.print("/etrave ");
  Serial.println(state);
}




